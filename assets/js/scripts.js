$(document).ready(function() {
	//Sliders
	$('.slider-principal').owlCarousel({
		nav: true,
		pagination: false,
		slideSpeed: 300,
		paginationSpeed: 400,
		items: 1,
		loop: true,
		navText: "",
		autoplay: true,
		autoplayTimeout: 5000
	});		
	$('.slider-publicacoes').owlCarousel({
		nav : true,
		pagination: false,
		slideSpeed : 300,
		paginationSpeed : 400,
		items: 4,
		navText: "",
		margin: 15,
		responsiveClass:true,
		responsive:{
			0:{ items:1 },
			576:{ items:2 },
			768:{ items:3 },
			992:{ items:4 }
		}
	});	
	$(".verticalCarousel").verticalCarousel({
		currentItem: 1,
		showItems: 4
	});
	
	//Menu
	$('.btn-abrir').click(function() {
			$('#mask').fadeIn();
			$('#nav').addClass("ativo");
			$('body').addClass("menu-ativo");
			$('.btn-fechar').addClass("ativo");
	});
	$('.btn-fechar').click(function() {
			$('#mask').fadeOut();
			$('#nav').removeClass("ativo");
			$('body').removeClass("menu-ativo");
			$('.btn-fechar').removeClass("ativo");
	});
	
	;
	//Scroll
	$(function () {
		var scrollDiv = document.createElement("div");
		$(scrollDiv).attr("id", "toTop").html("Voltar ao topo").appendTo("body");
		$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$("#toTop").fadeIn();
			} else {
				$("#toTop").fadeOut();
			}
		});
		$("#toTop").click(function () {
			$("body,html").animate({ scrollTop: 0 }, 800);
		});
	});
	
	//Enviar a um amigo
	$("#enviar-amigo").click(function() {
		$("#mask").fadeIn();
		$("#modal").fadeIn();
	});
	$("#fechar").click(function() {
		$("#mask").fadeOut();
		$("#modal").fadeOut();
	});
	
	//Campos date
	$(".datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'
	});
	
	//Mask
	$(".telefone").mask("(00)0000-00000");
	$(".accordion").accordion({
		heightStyle: "content",
		activate: function( event, ui ) {
			if(!$.isEmptyObject(ui.newHeader.offset())) {
				$('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
			}
		}
    });
	
});